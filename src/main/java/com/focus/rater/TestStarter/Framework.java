package com.focus.rater.TestStarter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.BeforeSuite;

import com.focus.rater.objects.GenericResponse;
import com.focus.rater.objects.NamedObjectInt;
import com.focus.rater.objects.dataTable.DataTableContext;
import com.focus.rater.objects.dataTable.Field;
import com.focus.rater.objects.dataTable.OptionsList;
import com.focus.rater.objects.RatebookContext;
import com.focus.rater.objects.TestCase;

public class Framework {
	
	protected HashMap<String, NamedObjectInt> objectMap;
	protected HashMap<String, DataTableContext> dataTableContext;
	protected HashMap<String, OptionsList> optionsMap;
	protected List<TestCase> testResults;
	protected RatebookContext ratebook;
	
	protected String environment;
	
	@BeforeSuite
	public void suiteConfigurationProcessor() {
		objectMap = new HashMap<>();
		
		optionsMap = new HashMap<>();
		dataTableContext = new HashMap<>();
		
		testResults = new ArrayList<>();
	}
	
	public GenericResponse execute() {
		
		// Build out all options for test cases
		testResults.addAll(generateTestCases(optionsMap, dataTableContext));
		if(testResults.size()==0) return new GenericResponse(false, "Test Cases Failed To Generate!");
		
		
		return new GenericResponse(true, "proof we hit end");
	}

	
	private List<TestCase> generateTestCases(HashMap<String, OptionsList> optMap2,
			HashMap<String, DataTableContext> dtContext2) {
		List<TestCase> testCases = new ArrayList<>();
		
		for(String dataTable : dtContext2.keySet()) {
			generateTestData(testCases, dtContext2.get(dataTable).getCatList(), new HashMap<String, Field>(), optMap2);
		}
		
		
		return testCases;
	}

	// This is the processor for generating test data
	private void generateTestData(List<TestCase> testCases, List<String> catList, HashMap<String, Field> hashMap,
			HashMap<String, OptionsList> optMap2) {
		// TODO Auto-generated method stub
		
	}

}
