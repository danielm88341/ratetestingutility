package com.focus.rater.functions;

import java.util.HashMap;

import com.focus.rater.objects.dataTable.DataTable;
import com.focus.rater.objects.dataTable.Field;

public class TableFunctions {
	
public static Field lookUpFieldMatch(DataTable data, HashMap<String, Field> headers) {
		DataTable local = data;
		for(String header : headers.keySet()) {
			local = local.extractSubGroup(header, headers.get(header));
		}
		// Add warning: if local.tableEntries.size() != 1
		// Add warning: if local.setFields.keySet.size() != headerNames.size()
		if(local!=null && local.getTableField(0)!=null) {
			return local.getTableField(0);
		} else {
			return new Field("NULL");
		}
	}
	
	public static Field lookUpFieldMatch(DataTable data, String headerName, Field headerField) {
		DataTable local = processTableLookup(data,headerName, headerField);
		// Add warning: if local.tableEntries.size() != 1
		// Add warning: if local.setFields.keySet.size() != headerNames.size()
		if(local!=null && local.getTableField(0)!=null) {
			return local.getTableField(0);
		} else {
			return new Field("NULL");
		}
	}
	
	private static DataTable processTableLookup(DataTable data, String headerName, Field headerField) {
		if(data.hasHeader(headerName)) {
			return data.extractSubGroup(headerName, headerField);			
		}
		return null;
	}
	
	public static Field lookUpFieldLessThan(DataTable data, HashMap<String, Field> headers) {
		// TODO Build this
		return null;
	}
	public static Field lookUpFieldGreaterThan(DataTable data, HashMap<String, Field> headers) {
		// TODO Build this
		return null;
	}

}
