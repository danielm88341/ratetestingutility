package com.focus.rater.init;

import org.springframework.context.ApplicationContext;

public class AppContext {

	private InitVariables initVariables;
	private ApplicationContext springAppContext;
	private static ThreadLocal<AppContext> thisThreadLocal = new ThreadLocal<AppContext>();
	
	public static void setInitAppContext(InitVariables initVariables, ApplicationContext springAppContext) {
		AppContext ctx = new AppContext();
		ctx.setInitVariables(initVariables);
		ctx.setSpringAppContext(springAppContext);
		thisThreadLocal.set(ctx);
	}

	public InitVariables getInitVariables() {
		return initVariables;
	}

	public void setInitVariables(InitVariables initVariables) {
		this.initVariables = initVariables;
	}

	public ApplicationContext getSpringAppContext() {
		return springAppContext;
	}

	public void setSpringAppContext(ApplicationContext springAppContext) {
		this.springAppContext = springAppContext;
	}
	
}
