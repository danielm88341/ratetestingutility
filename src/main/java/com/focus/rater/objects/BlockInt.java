package com.focus.rater.objects;

import java.util.HashMap;

import com.focus.rater.objects.dataTable.Field;

public interface BlockInt{
	
	/**
	 * Call this when performing test case validation
	 * @param data input data to validate against
	 * @param context premium data to test against
	 * @return status of whether validation passes or not
	 */
	public TestCase validateForInputs(HashMap<String, Field> data, PremiumContext context);
	/**
	 * Call this when calculating test data
	 * @param data test data
	 * @return Field data for result (field internal value can be any data type)
	 */
	public Field calculateFunction(HashMap<String, Field> data);
	

}
