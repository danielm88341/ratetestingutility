package com.focus.rater.objects;

public class GenericResponse {

	private boolean pass;
	private String message;
	
	public GenericResponse() {
		this.pass = false;
		this.message = "";
	}
	public GenericResponse(boolean status) {
		this.pass = status;
		this.message = "";
	}
	public GenericResponse(boolean status, String message) {
		this.pass = status;
		this.message = message;
	}
	public boolean isPass() {
		return pass;
	}
	public void setPass(boolean pass) {
		this.pass = pass;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
