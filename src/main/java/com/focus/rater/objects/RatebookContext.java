package com.focus.rater.objects;

import java.util.ArrayList;
import java.util.List;

public abstract class RatebookContext implements NamedObjectInt {
	
	protected String client;
	protected String ratebookName;	// use this as name
	protected String LOB;
	protected String polForm;
	protected String state;
	protected String defaultRiskID;
	protected List<String> perils;
	protected List blocksList;
	protected List<String> resourceRef = new ArrayList<>();
	
	@Override
	public String getName() {
		return ratebookName;
	}


	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}


	public String getRatebookName() {
		return ratebookName;
	}
	public void setRatebookName(String ratebookName) {
		this.ratebookName = ratebookName;
	}


	public String getLOB() {
		return LOB;
	}
	public void setLOB(String lOB) {
		LOB = lOB;
	}


	public String getPolForm() {
		return polForm;
	}
	public void setPolForm(String polForm) {
		this.polForm = polForm;
	}


	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}


	public List getBlocksList() {
		return blocksList;
	}
	public void setBlocksList(List blocksList) {
		this.blocksList = blocksList;
	}


	public List<String> getResourceRef() {
		return resourceRef;
	}
	public void setResourceRef(List<String> resourceRef) {
		this.resourceRef = resourceRef;
	}
	
	
	public List<String> getPerils() {
		return perils;
	}
	public void setPerils(List<String> perils) {
		this.perils = perils;
	}


	public String getDefaultRiskID() {
		return defaultRiskID;
	}


	public void setDefaultRiskID(String defaultRiskID) {
		this.defaultRiskID = defaultRiskID;
	}
}
