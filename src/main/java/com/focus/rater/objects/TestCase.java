package com.focus.rater.objects;

import java.math.BigDecimal;
import java.util.HashMap;

import com.focus.rater.objects.dataTable.Field;

public class TestCase extends GenericResponse {
	
	HashMap<String, Field> testCaseDetails;
	Field premResult;
	Field calcResult;
	
	public TestCase(HashMap<String, Field> testCaseDetails) {
		super();
		this.testCaseDetails = testCaseDetails;
	}
	private TestCase(TCBuilder builder) {
		super();
		this.premResult = builder.premResult;
		this.calcResult = builder.calcResult;
		this.testCaseDetails = builder.testCaseDetails;
	}
	
	public void setTestCaseDetails(HashMap<String, Field> testCaseDetails) {
		this.testCaseDetails = testCaseDetails;
	}
	public void setPremResult(Field premResult) {
		this.premResult = premResult;
	}
	public Field getPremResult() {
		return premResult;
	}
	public void setCalcResult(Field calcResult) {
		this.calcResult = calcResult;
	}
	public Field getCalcResult() {
		return calcResult;
	}
	
	public static class TCBuilder{
		HashMap<String, Field> testCaseDetails;
		final Field premResult;
		final Field calcResult;
		
		public TCBuilder(String header, Field value) {
			testCaseDetails = new HashMap<>();
			testCaseDetails.put(header, value);
			premResult = new Field(BigDecimal.ZERO);
			calcResult = new Field(BigDecimal.ONE);
		}
		public TCBuilder addTC(String header, Field value) {
			testCaseDetails.put(header, value);
			return this;
		}
		public TestCase build() {
			return new TestCase(this);
		}
	}

}
