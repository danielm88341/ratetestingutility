package com.focus.rater.objects.blocks;

import java.util.HashMap;
import java.util.List;

import com.focus.rater.functions.TableFunctions;
import com.focus.rater.objects.BlockInt;
import com.focus.rater.objects.GenericResponse;
import com.focus.rater.objects.NamedObjectInt;
import com.focus.rater.objects.PremiumContext;
import com.focus.rater.objects.TestCase;
import com.focus.rater.objects.dataTable.DataTable;
import com.focus.rater.objects.dataTable.Field;

public class DataTableLookup implements BlockInt, NamedObjectInt {
	
	/*
	 * Notes:
	 * other blocks will reference this by looking up the block name (getName()) and performing calculateFunction(...)
	 * TBD: do we need to map input fields here?
	 * TBD: when using dataName: how will the dataTable be looped in after the fact?
	 */
	
	private String blockName;
	private DataTable data;
	private String dataName;	// match to reference file
	private String premiumField;
	
	public DataTableLookup(String blockName, String dataName, String premiumField) {
		this.blockName = blockName;
		this.dataName = dataName;
		this.premiumField = premiumField;
	}
	
	// TODO Rebuild to build from DataTableContext
	
	/*public DataTableLookup(String blockName, DataTable data, String premiumField) {
		this.blockName = blockName;
		this.data = data;
		this.premiumField = premiumField;
	}*/

	@Override
	public String getName() {
		return blockName;
	}

	
	@Override
	public TestCase validateForInputs(HashMap<String, Field> headerData, PremiumContext context) {
		TestCase results = new TestCase(headerData);
		
		results.setCalcResult(calculateFunction(headerData));
		results.setPremResult(context.getPremiumValue(premiumField));
		
		if(results.getCalcResult().matches(results.getPremResult())) {
			results.setPass(true);
			results.setMessage("Value Matches");
		} else {
			results.setMessage("Value mismatch detected ["+results.getCalcResult().toString()+"] vs ["+results.getPremResult().toString()+"]");
		}
		
		return results;
	}
	
	@Override
	public Field calculateFunction(HashMap<String, Field> headerData) {
		return TableFunctions.lookUpFieldMatch(data, headerData);
	}

}
