package com.focus.rater.objects.dataTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.focus.rater.objects.GenericResponse;
import com.focus.rater.objects.NamedObjectInt;

public class DataTable implements NamedObjectInt {
	
	protected String tableName;
	protected List<String> headerNames;
	protected List<Row> tableEntries;
	protected HashMap<String, Field> setFields;
	
	public DataTable(String tableName) {
		this.tableName = tableName;
		this.headerNames = new ArrayList<>();
		this.tableEntries = new ArrayList<>();
		this.setFields = new HashMap<String, Field>();
	}
	public DataTable(String tableName, List<String> headerNames) {
		this(tableName);
		this.headerNames.addAll(headerNames);
	}
	
	public List<OptionsList> extractOptions(){
		List<OptionsList> optionsList = new ArrayList<OptionsList>();
		
		for(String header : headerNames) {
			optionsList.add(new OptionsList(header));
		}
		
		for(Row row : tableEntries) {
			for(String header : headerNames) {
				for(OptionsList opt : optionsList) {
					if(opt.getName().equals(header) && !opt.containsField(row.getHeaderField(header))) {
						opt.addField(row.getHeaderField(header));
					}
				}
			}
		}
		
		return optionsList;
	}
	
	public DataTable extractSubGroup(String header, Field headerField) {
		// initialize extraction
		DataTable extract = new DataTable(this.tableName, this.headerNames);
		extract.setFields.putAll(this.setFields);
		if(!this.headerNames.contains(header)) return extract;
		
		// Process extraction
		extract.setFields.put(header, headerField);
		for(Row row : this.tableEntries) {
			if(row.matchesHeader(header, headerField)) {
				extract.tableEntries.add(row);
			}
		}
		return extract;
	}
	
	public GenericResponse loadTable(String input, String dataHeader) {
		if(input==null || dataHeader==null) {
			return new GenericResponse(false,"Insuficient input information:"+(input==null?"[Input=null]":"")+(dataHeader==null?"[dataHeader==null]":""));
		}
		String[] rows = input.split("\n");
		if(rows.length<2) return new GenericResponse(false, "Insuficient data in input (needs more rows)");
		
		// Always assume row 1 is the header information
		// Process assumes that "|" is used to denote headers from fields
		// Process assumes that all sections of data are CSV
		ArrayList<String> rowHeaders = new ArrayList<>();
		String row;
		Row rowData;
		String[] rowSplit;
		GenericResponse resp = new GenericResponse();
		for(int i = 0; i < rows.length; i++) {
			row = rows[i];
			String[] split = row.split("|");
			rowSplit = split[1].split(",");
			
			// first row: perform initialization
			if(i==0) {
				for(String s : split[0].split(",")) headerNames.add(s);
				for(String s : rowSplit) rowHeaders.add(s);
				headerNames.add(dataHeader);
				continue;
			}
			
			if(split[0].split(",").length!=(headerNames.size()-1)) {
				resp.setMessage(resp.getMessage()+"\n"+"Insuficient column data in row "+i+" Skipping row");
				continue;
			}
			if(split[1].split(",").length!=rowHeaders.size()) {
				resp.setMessage(resp.getMessage()+"\n"+"Insuficient field data in row "+i+" Skipping row");
				continue;
			}
			
			for(int j = 0; j < rowSplit.length; j++) {
				rowData = new Row(new Field(rowSplit[j]));
				rowData.addHeader(dataHeader, new Field(rowHeaders.get(j)));
				for(int k = 0; k <headerNames.size()-1; k++) {
					rowData.addHeader(headerNames.get(k), new Field(split[0].split(",")[k]));
				}
				tableEntries.add(rowData);
			}
			
		}
		
		if(resp.getMessage().length()==0) {
			resp.setMessage("Successfully loaded table "+tableName);
			resp.setPass(true);
		} else {
			resp.setMessage(resp.getMessage().trim());
		}
		return resp;
		
	}
	
	public boolean hasHeader(String header) {
		if(headerNames.contains(header)) {
			return true;
		}
		return false;
	}
	public int getTableSize() {
		return tableEntries.size();
	}
	public Field getTableField(int i) {
		if(i<0 || i >= tableEntries.size()) return null;
		return tableEntries.get(i).getValue();
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return tableName;
	}
	
	
	public class Row{
		HashMap<String, Field> headers;
		Field value;
		
		Row(){
			value = null;
			headers = new HashMap<>();
		}
		public Row(Field field) {
			this();
			this.value = field;
		}
		public Row(Field field, String headerName, Field headerField) {
			this(field);
			headers.put(headerName, headerField);
		}
		/**
		 * Builder function for adding multiple cell headers at one time.
		 * @param headerName
		 * @param headerField
		 * @return
		 */
		public Row addHeader(String headerName, Field headerField) {
			headers.put(headerName, headerField);
			return this;
		}
		public Field getHeaderField(String headerName) {
			if(headers.containsKey(headerName)) return headers.get(headerName);
			return null;
		}
		/**
		 * Function for matching headers
		 * @param headerName
		 * @param headerField
		 * @return
		 */
		public boolean matchesHeader(String headerName, Field headerField) {
			if(value==null) return false;
			if(headers.containsKey(headerName)
					&& headers.get(headerName).matches(headerField)) {
				return true;
			}
			return false;
		}
		public Field getValue() {
			return value;
		}
	}

}
