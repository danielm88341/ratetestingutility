package com.focus.rater.objects.dataTable;

import java.util.ArrayList;
import java.util.List;

public class DataTableContext {
	
	/*
	 * Items needed:
	 * - Name ID
	 * - Named table reference file
	 * - Category Input List (reference OptionsList names)
	 * 
	 * System data validation requirements (save for notes, do not implement yet)
	 * - Risk Input List
	 * - Premium Input (final step of the output)
	 * 
	 */
	
	// Test Data requirements
	protected String name;
	protected String resourceName;
	protected List<String> catList;
	// System testing requirements
	//protected List<String> riskInputList;
	//protected String premiumInput;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public List<String> getCatList() {
		return catList;
	}
	public void setCatList(List<String> catList) {
		this.catList = catList;
	}
	
	private DataTableContext(DTBuilder builder) {
		this.name = builder.name;
		this.resourceName = builder.resourceName;
		this.catList = builder.catList;
	}
	
	public static class DTBuilder {
		private final String name;
		private final String resourceName;
		private List<String> catList;
		
		public DTBuilder(String name, String resourceName) {
			this.name = name;
			this.resourceName = resourceName;
			catList = new ArrayList<>();
		}
		public DTBuilder addCat(String catName) {
			catList.add(catName);
			return this;
		}
		public DataTableContext build() {
			return new DataTableContext(this);
		}
	}
	
	

}
