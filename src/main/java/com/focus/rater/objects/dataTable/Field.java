package com.focus.rater.objects.dataTable;

import java.math.BigDecimal;

public class Field {
	
	String fieldString;
	BigDecimal fieldBD;
	Integer fieldInt;
	
	public Field(String fieldString){
		this.fieldString = fieldString;
	}
	public Field(BigDecimal fieldBD){
		this.fieldBD = fieldBD;
	}
	public Field(Integer fieldInt){
		this.fieldInt = fieldInt;
	}
	
	public boolean isString() {
		return fieldString!=null;
	}
	public boolean isNumber() {
		return fieldBD !=null || fieldInt!=null;
	}
	public boolean isInt() {
		return fieldInt!=null;
	}
	public boolean isBigDecimal() {
		return fieldBD!=null;
	}
	
	public String getString() {
		if(fieldString!=null) {
			return fieldString;
		}
		if(fieldInt!=null) {
			return fieldInt.toString();
		}
		if(fieldBD!=null) {
			return fieldBD.toString();
		}
		return null;
	}
	public Number getNumber() {
		if(fieldInt!=null) {
			return fieldInt;
		}
		if(fieldBD!=null) {
			return fieldBD;
		}
		return null;
	}
	public Integer getInteger() {
		if(fieldInt!=null) return fieldInt;
		if(fieldString!=null) {
			try {
				return Integer.parseInt(fieldString.replace(",",""));
			} catch (Exception e) {
				// Do nothing
			}
		}
		if(fieldBD!=null) {
			try {
				return fieldBD.intValueExact();
			} catch (Exception e) {
				// Do nothing
			}
		}
		return null;
	}
	
	public boolean matches(Field other) {
		if(other==null) return false;
		
		if(isString() && other.isString()) {
			return this.fieldString.equals(other.getString());
		}
		if(isInt() && other.isInt()) {
			return this.fieldInt==other.fieldInt;
		}
		if(isBigDecimal() && other.isBigDecimal()) {
			return this.fieldBD.compareTo(other.fieldBD)==0;
		}
		
		return false;
	}
	
}
