package com.focus.rater.objects.dataTable;

import java.util.ArrayList;
import java.util.Arrays;

import com.focus.rater.objects.NamedObjectInt;

/**
 * Class for tracking submit-able options independent of a dataTable
 * @author Daniel.Mills
 * TODO Figure out how to restrict this for individual executions (i.e. Calculating peril values)
 */
public class OptionsList implements NamedObjectInt{
	
	String optionName;
	ArrayList<Field> options;
	Field defaultValue;
	
	public OptionsList(String optionName) {
		this.optionName = optionName;
		options = new ArrayList<>();
	}
	public OptionsList(Field defaultValue, String optionName, Field...fields) {
		this.optionName = optionName;
		this.defaultValue = defaultValue;
		options = new ArrayList<>(Arrays.asList(fields));
	}
	public OptionsList(String optionName, Field...fields) {
		this.optionName = optionName;
		this.defaultValue = null;
		this.options = new ArrayList<>(Arrays.asList(fields));
	}
	
	@Override
	public String getName() {
		return optionName;
	}
	
	public void addField(Field field) {
		options.add(field);
	}
	public Field getField(int i) {
		if(i<0 || i>=options.size()) {
			return null;
		}
		return options.get(i);
	}
	public int getFieldsCount() {
		return options.size();
	}
	public boolean containsField(Field field) {
		for(Field opt : options) {
			if(opt.matches(field)) return true;
		}
		return false;
	}
	
	public void setDefaultField(Field field) {
		this.defaultValue = field;
	}
	/**
	 * Returns a default value for non-testing use<br>
	 * if defaultValue is not set, returns either the<br>
	 * first field in options or Field("NULL")
	 * @return
	 */
	public Field getDefaultField() {
		if(defaultValue!=null) return defaultValue;
		if(options.size()>0) return options.get(0);
		return new Field("NULL");
	}
	

}
