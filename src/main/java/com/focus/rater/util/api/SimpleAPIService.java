package com.focus.rater.util.api;

import java.net.URI;
import java.util.HashMap;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SimpleAPIService {
	
	private Integer connectTimeout = 700;
	private Integer readTimeout = 3000;
	
	URI baseUri;
	String path;
	HTTPBasicAuthFilter auth;
	MultivaluedMap<String,String>  queryParamMap;
	HashMap<String,Object> headerMap;
	MediaType acceptType;
	MediaType contentType;
	
	WebResource webResource;

	public SimpleAPIService(String baseURL) {
		baseUri = UriBuilder.fromPath(baseURL).build();
	}
	
	public SimpleAPIService setPath(String path) {
		this.path = path;
		return this;
	}
	public SimpleAPIService addQueryParam(String key, String value) {
		if(queryParamMap==null) queryParamMap = new MultivaluedMapImpl();
		queryParamMap.add(key, value);
		return this;
	}
	public SimpleAPIService addHeader(String key, Object value) {
		if(headerMap==null) headerMap= new HashMap<>();
		headerMap.put(key, value);
		return this;
	}
	public SimpleAPIService addAuth(String user, String password) {
		this.auth = new HTTPBasicAuthFilter(user, password);
		return this;
	}
	public SimpleAPIService setAccept(MediaType accept) {
		this.acceptType = accept;
		return this;
	}
	public SimpleAPIService setContent(MediaType content) {
		this.contentType=content;
		return this;
	}
	
	public SimpleAPIService build() {
		Client clientC = Client.create(new DefaultClientConfig());
		clientC.setConnectTimeout(connectTimeout);
		clientC.setReadTimeout(readTimeout);
		if(auth!=null) clientC.addFilter(auth);
		if(path==null) {
			webResource = clientC.resource(baseUri);
		} else {
			webResource = clientC.resource(baseUri).path(path);
		}
		if(queryParamMap!=null) webResource = webResource.queryParams(queryParamMap);
		if(headerMap!=null) {
			for(String key : headerMap.keySet()) webResource.header(key, headerMap.get(key));
		}
		if(acceptType!=null) webResource.getRequestBuilder().accept(acceptType);
		if(contentType!=null) webResource.getRequestBuilder().type(contentType);
		
		return this;
	}
	
	public ClientResponse get() throws Exception {
		if(webResource==null) {
			throw new Exception("Failed to build service before execution.");
		}
		return webResource.get(ClientResponse.class);
	}
	public ClientResponse post(String payload) throws Exception {
		if(webResource==null) {
			throw new Exception("Failed to build service before execution.");
		}
		if(payload==null) {
			return webResource.post(ClientResponse.class);
		} else {
			return webResource.post(ClientResponse.class, payload);
		}
	}
	
}
