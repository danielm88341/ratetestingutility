package com.focus.rater.example;

import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.focus.rater.TestStarter.Framework;
import com.focus.rater.objects.GenericResponse;
import com.focus.rater.objects.blocks.DataTableLookup;
import com.focus.rater.objects.dataTable.Field;
import com.focus.rater.objects.dataTable.OptionsList;
import com.focus.rater.objects.dataTable.DataTableContext;
import com.focus.rater.service.FSI.FSI_FSIFLHO18B01;

public class FSITableLookup extends Framework{
	
	@Test
	@Parameters({"environment", "riskIDOverride"})
	public void testTableLookup(@Optional("beta") String environment, @Optional("FSIHO00000") String riskIDOverride) {
		this.ratebook = new FSI_FSIFLHO18B01("HO6");
		this.ratebook.getResourceRef().add("example/FSI/PCForms.txt");
		if(riskIDOverride!=null) this.ratebook.setDefaultRiskID(riskIDOverride);
		
		/*
		 *  Conceptually, each test case will manage the reduced test case list
		 *  but the ratebook object will spawn the full list
		 */
		// TBD: Multi-peril: trigger per line or is it possible to work it through automatically by boolean trigger?
		//	-> Build out full. Simplify later
		//this.objectMap.put("basicTableLU", new DataTableLookup("PCForms", "PCForms", "PCFactor"));
		
		this.optionsMap.put("Perils", new OptionsList("Perils", new Field("Theft"), new Field("AOP"), new Field("Hurr"), new Field("OW")));
		this.optionsMap.put("CONN", new OptionsList(new Field("F"), "CONN", new Field("F"), new Field("A"), new Field("P"), new Field("M"), new Field("V"), new Field("S")));
		this.optionsMap.put("PC", new OptionsList(new Field(1), "PC", new Field(1), new Field(2), new Field(3), new Field(4), new Field(5), new Field(6), new Field(7), new Field(8), new Field(9), new Field(10)));
		
		this.dataTableContext.put("PCForms", new DataTableContext.DTBuilder("PCForms", "PCForms").addCat("Perils").addCat("PC").addCat("CONN").build());
		
		this.environment = environment;
		
		GenericResponse result = this.execute();
		Assert.assertTrue(result.isPass(), result.getMessage());
	}

}
